﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Matching_Pairs
{
    class Card
    {
        // ---------------------
        // Type Definitions
        // ---------------------
        public enum Symbols
        // A special type of integer with only speicific values allowed, that have names
        {
            HAND,  // = 0
            KNIFE,       // = 1
            TRIFORCE,   // = 2
            GHOST,      // = 3
            AXE,        // = 4

            // --

            NUM         // = 5
        }
        // ---------------------
        // Data
        // ---------------------
        Texture2D image, front = null;
        SoundEffect sound = null;
        Vector2 position = Vector2.Zero;
        bool flipped = false;

        // ---------------------
        // Behaviour
        // ---------------------

        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/backOfCard");

            front = content.Load<Texture2D>("graphics/frontOfCard");
        }
        // ---------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (flipped == false)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
            else

            if (flipped == true)
            {
                spriteBatch.Draw(front, position, Color.White);
            }
        }
        // ---------------------
        public void Input()
        {

            // Get the current status of the mouse
            MouseState currentstate = Mouse.GetState();

            // Get the bounding box of the card
            Rectangle bounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            // Check if we have the left mouse button held down over the card
            // if the card has already been flipped
            if (currentstate.LeftButton == ButtonState.Pressed
                && bounds.Contains(currentstate.X, currentstate.Y)
                && flipped == false)
            {
                // We Click the Card

                //sound.Play(); Yet to be implemented

                //Turn the card over
                flipped = true;

            }
    
        }
        // ---------------------
        /*public void Show()
        {
            flipped = false;
        }
        */
        // ---------------------


    }
}
